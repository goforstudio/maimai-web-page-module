var mai_data = [];
//加载data.xml
function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      load_Mai_Weather(this);
    }
  };
  xhttp.open("GET", "/xml/data.xml", true);
  xhttp.send();
}
function load_Mai_Weather(xml) {

  //console.log(xml);
  var xmlDoc = xml.responseXML;
  //console.log(xmlDoc);
  var x = xmlDoc

  console.log(x.getElementsByTagName('city')[0].innerHTML);
  //城市
  mai_data[0] = x.getElementsByTagName('city')[0].innerHTML;
  document.querySelector("#location").innerHTML = mai_data[0];
  //天气
  mai_data[1] = x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('day')[0].getElementsByTagName('type')[0].innerHTML;
  document.querySelector("#w_icon").setAttribute("src", "/img/w_icon/" + mai_data[1] + ".png");
  document.querySelector("#wz").innerHTML = mai_data[1];
  //温度
  mai_data[2] = x.getElementsByTagName('wendu')[0].innerHTML;
  document.querySelector("#temperature").innerHTML = mai_data[2];
  //时间
  mai_data[3] = x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('date')[0].innerHTML
  document.querySelector("#time").innerHTML = mai_data[3];
  //湿度
  mai_data[4] = x.getElementsByTagName('shidu')[0].innerHTML;
  document.querySelector("#humidity").innerHTML = mai_data[4];
  //风向
  mai_data[5] = x.getElementsByTagName('fengxiang')[0].innerHTML;
  document.querySelector("#wind").innerHTML = mai_data[5];
  //最高最低温度
  mai_data[6] = x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('high')[0].innerHTML;
  mai_data[7] = x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('low')[0].innerHTML;
  document.querySelector("#temperature1").innerHTML = mai_data[6].split(' ')[1];
  document.querySelector("#temperature2").innerHTML = mai_data[7].split(' ')[1];
  //白昼天气
  mai_data[8] = x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('day')[0].getElementsByTagName('type')[0].innerHTML;
  mai_data[8] += ' ' + x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('day')[0].getElementsByTagName('fengxiang')[0].innerHTML;
  //mai_data[8]+=' '+x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('day')[0].getElementsByTagName('fengli')[0].innerHTML;
  document.querySelector("#weather_day").innerHTML = mai_data[8];
  //夜间天气
  mai_data[9] = x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('night')[0].getElementsByTagName('type')[0].innerHTML;
  mai_data[9] += ' ' + x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('night')[0].getElementsByTagName('fengxiang')[0].innerHTML;
  //mai_data[8]+=' '+x.getElementsByTagName('forecast')[0].getElementsByTagName('weather')[0].getElementsByTagName('night')[0].getElementsByTagName('fengli')[0].innerHTML;
  document.querySelector("#weather_night").innerHTML = mai_data[9];
  //日出日落
  mai_data[10]=x.getElementsByTagName('sunrise_1')[0].innerHTML;
  mai_data[11]=x.getElementsByTagName('sunset_1')[0].innerHTML;
  document.querySelector("#sunrise").innerHTML=mai_data[10];
  document.querySelector("#sunset").innerHTML=mai_data[11];
  //指数
  mai_data[12]=x.getElementsByTagName('zhishus')[0].getElementsByTagName('zhishu')[10].getElementsByTagName('value')[0].innerHTML;
  mai_data[12]+=" "+x.getElementsByTagName('zhishus')[0].getElementsByTagName('zhishu')[10].getElementsByTagName('detail')[0].innerHTML
  document.querySelector("#comfort").innerHTML=mai_data[12];
}
//迈迈我真的好喜欢你啊！为了你我要学JS！！！我要学读取XML文档！！！！
loadDoc();