﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    public static string GetHttpResponse(string url, int Timeout)
    {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        request.Method = "GET";
        request.ContentType = "text/html;charset=UTF-8";
        request.UserAgent = null;
        request.Timeout = Timeout;

        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        Stream myResponseStream = response.GetResponseStream();
        StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
        string retString = myStreamReader.ReadToEnd();
        myStreamReader.Close();
        myResponseStream.Close();

        return retString;
    }

    public static void Write_Xml(string xml_str)
    {
        using (StreamWriter sw = new StreamWriter("F:/学习/毕业设计/产品/maimai-web-page-module/data.xml",true))
        {
                
                sw.Write(xml_str);

        }

        //// 从文件中读取并显示每行
        //string line = "";
        //using (StreamReader sr = new StreamReader("names.txt"))
        //{
        //    while ((line = sr.ReadLine()) != null)
        //    {
        //        Console.WriteLine(line);
        //    }
        //}
        //Console.ReadKey();

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //string cid = Request.Params["cid"];
        byte[] data_xml;
        //if (cid == null)
        //{
        //    Response.Redirect("Default.aspx?Error=非法操作！code3");
        //}
        //else
        //{
        //    data_xml = GetHttpResponse("http://wthrcdn.etouch.cn/WeatherApi" + "?citykey=" + cid, 2000);
        //    Write_Xml(data_xml);
        //}
        data_xml = GetHttpResponse("http://wthrcdn.etouch.cn/WeatherApi" + "?citykey=101010100", 2000);
 
        string inputString = System.Text.Encoding.UTF8.GetString(data_xml);
        Write_Xml(inputString);
    }
}