﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="/res/css/weather.css" />
    <title>迈迈天气</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1 style="text-align: center;">迈迈天气</h1>
            <div class="box1">
                <div class="box2">
                    <h1 class="tt" id="location">西安 长安</h1>
                    <div class="box3">
                        <div class="box4">
                            <img src="res/img/w_icon/01.png" width="150" height="150" />
                            <p>温度</p>
                            <p><a id="temperature">3</a>℃</p>
                            <p><a id="time">12日 星期三</a></p>
                        </div>
                        <div class="box5">
                            <p>湿度<a id="humidity">36%</a></p>
                            <p>风向<a id="wind">东风</a></p>
                            <p>最高温度：<a id="temperature1">10℃</a>&emsp;最高温度：<a id="temperature2">-4℃</a></p>
                            <p>白昼：<a id="weather1"> 多云 西风</a>&emsp;夜间：<a id="weather2"> 多云 西风</a></p>
                            <p><a id="comfort">舒适度： 极不舒适 天气湿冷，舒适感很低，风湿患者更要做好防护。</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div style="text-align: center;">
                <p>站长：<a href="https://space.bilibili.com/6070425">不折不扣007</a> ICP证：<a href="http://www.beian.miit.gov.cn">陕19009607号</a></p>
            </div>

        </div>
    </form>
</body>
</html>
